# Se utiliza una imagen base de Python
FROM python:3.8-slim

# Se instalan las dependencias desde el archivo requirements.txt
RUN pip install Flask google-cloud-storage opencv-python-headless Pillow python-dotenv

# Se crea un nuevo grupo de sistema(flag -r) llamado "user" 
RUN groupadd -r user

# Se crea un nuevo usuario de sistema(flag -r) y se agrega al grupo creado previamente (flag -g)
RUN useradd -r -g user user


# Se establece el directorio de trabajo en el contenedor
WORKDIR /app

# Se cambian los npermisos del workdir al usuario no root "user"
RUN chown -R user:user /app

# Se cambia al usuario no root "user"
USER user 

# Copia el código de la aplicación al contenedor
COPY . .

# Define el comando para ejecutar la aplicación
CMD ["python", "Filter.py"]
