import os
import json
import shutil
import logging
from google.cloud import storage
from flask import Flask, request, jsonify
import cv2
from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


# Especifica la ruta al archivo de credenciales descargado
credentials_path = '/etc/secret-volume/sdpp2-405215-a21bf250e80c.json'

# Crea un cliente de almacenamiento de Google Cloud
storage_client = storage.Client.from_service_account_json(credentials_path)

# Cargando las variables de entorno desde un archivo .env
from dotenv import load_dotenv
load_dotenv()

app = Flask(__name__)
port = int(os.environ.get("PORT", 4545))
host = os.environ.get("HOST", '0.0.0.0')

def create_directories():
    directories = ['./output', './frames', './logs']
    for directory in directories:
        if not os.path.exists(directory):
            os.makedirs(directory)

@app.route('/filter', methods=['POST'])
def filter_video():
    try:
        data = request.get_json()

        video_id = data.get('videoID')
        bucket_name = video_id
        frame_range = data.get('rango')

        app.logger.info(f'Procesando VideoID: {video_id} - Rango de trabajo: [{frame_range[0]} - {frame_range[1]}] frames.')

        # Descargar frames del bucket a un directorio local
        list_frames = download_frames_from_bucket(bucket_name, frame_range[0], frame_range[1], 'frames')

        # Procesar los frames
        filter_frames('frames', 'output', list_frames)

        # Subir los frames procesados de vuelta al bucket
        upload_files_to_bucket(bucket_name, 'output', list_frames)

        app.logger.info('Frames Procesados...')

        cleanup_frames(list_frames)
    
        return jsonify({}), 200

    except Exception as e:
        app.logger.error(f'Error al procesar el video: {str(e)}')
        return jsonify({"error": "Error al procesar el video"}), 500

def download_frames_from_bucket(bucket_name, start_frame, end_frame, local_directory):
    frame_names = []

    try:
        app.logger.info(f'Solicitando a GCP frames: [{start_frame} - {end_frame}]')
        bucket = storage_client.get_bucket(bucket_name)
        input_directory = 'input'

        if not os.path.exists(local_directory):
            os.makedirs(local_directory)

        for frame_number in range(start_frame, end_frame + 1):
            frame_name = f'frame{str(frame_number).zfill(4)}.png'
            download_frame_from_gcs(bucket, os.path.join(input_directory, frame_name), os.path.join(local_directory, frame_name))
            frame_names.append(frame_name)

        return frame_names

    except Exception as e:
        app.logger.error(f'Error al descargar frames: {str(e)}')
        return frame_names

def download_frame_from_gcs(bucket, frame_path_in_bucket, local_file_path):
    try:
        blob = bucket.blob(frame_path_in_bucket)
        with open(local_file_path, 'wb') as file:
            blob.download_to_file(file)
    except Exception as e:
        app.logger.error(f'Error al descargar el frame {frame_path_in_bucket}: {str(e)}')


def filter_frames(input_directory, output_directory, png_file_list):
    try:
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)

        app.logger.info('Aplicando filtro Sobel a los frames...')

        for file in png_file_list:
            input_file_path = os.path.join(input_directory, file)
            output_file_path = os.path.join(output_directory, file)

            # Leer la imagen en escala de grises
            image = cv2.imread(input_file_path, cv2.IMREAD_GRAYSCALE)

            if image is not None:
                # Aplicar el filtro Sobel
                sobel = cv2.Sobel(image, cv2.CV_64F, 1, 1, ksize=5)
                sobel = cv2.convertScaleAbs(sobel)

                # Guardar la imagen procesada en el directorio de salida
                name, ext = os.path.splitext(file)
                output_file_name = name + '.png'
                output_file_path = os.path.join(output_directory, output_file_name)
                cv2.imwrite(output_file_path, sobel)
            else:
                app.logger.error(f'Error al leer la imagen: {input_file_path}')

        app.logger.info('Filtro Sobel aplicado a los frames.')

    except Exception as e:
        app.logger.error(f'Error al procesar los frames: {str(e)}')


def upload_files_to_bucket(bucket_name, output_directory, png_file_list):
    try:
        app.logger.info('Subiendo frames procesados al bucket...')
        bucket = storage_client.get_bucket(bucket_name)

        for file in png_file_list:
            upload_to_bucket(bucket, file)

        app.logger.info('Todos los frames procesados se han subido al bucket.')

    except Exception as e:
        app.logger.error('Error al subir los archivos: ' + str(e))


def upload_to_bucket(bucket, dest_file_name):
    try:
        blob = bucket.blob(f'output/{dest_file_name}')
        blob.upload_from_filename(f'output/{dest_file_name}')
    except Exception as e:
        app.logger.error(f'Error al subir el archivo {dest_file_name} al bucket: {str(e)}')

# Función para eliminar los frames descargados y ya procesados.
def cleanup_frames(png_file_list):
    try:
        for file in png_file_list:
            # Ruta del archivo en el directorio /input
            input_file_path = os.path.join('/input', file)

            # Ruta del archivo en el directorio /output
            output_file_path = os.path.join('/output', file)

            # Eliminar el archivo si existe en /input
            if os.path.exists(input_file_path):
                os.remove(input_file_path)

            # Eliminar el archivo si existe en /output
            if os.path.exists(output_file_path):
                os.remove(output_file_path)

        app.logger.info("Frames descargados y procesados eliminados.")

    except Exception as e:
        app.logger.error(f'Error al eliminar archivos: {str(e)}')



if __name__ == '__main__':
    create_directories()
    app.logger.info("Iniciando Filter...")
    app.run(host=host, port=port)
